#pragma warning(disable : 4996)
#include <iostream>
#include <vector>
#include <string>
#include <cstdint>

struct ListNode
{
	ListNode* prev = nullptr;
	ListNode* next = nullptr;
	ListNode* rand = nullptr;
	std::string data;
};

class List
{
public:
	List( const std::vector<std::string>& List_Data_Mass )
	{
		head = nullptr;
		tail = nullptr;
		count = 0;

		auto Size = List_Data_Mass.size();

		for (int i = 0; i < Size; i++)
		{
			Add_Item(List_Data_Mass[i]);
		}
	}

	List()
	{
		//Add_Item("");
	}

	void Serialize(FILE* file)
	{
		if (head == nullptr)
		{
			return;
		}

		ListNode* temp = head;
		int64_t N;
		fwrite(&count, sizeof( int64_t ), 1, file);

		for (int i = 0; i < count; i++)
		{
			N = temp->data.length();
			fwrite(&N, sizeof( int64_t ), 1, file);
			fwrite(temp->data.c_str(), N * sizeof(char), 1, file);
			if (temp->next != nullptr)
			{
				temp = temp->next;
			}
		}
		
	}

	void Deserialize(FILE* file)
	{
		if (fgetc(file) == 0)
		{
			return;
		}

		ListNode* temp = head;
		int64_t N;

		fseek(file, 0, SEEK_SET);

		int64_t curCouunt = 0;
		fread(&curCouunt, sizeof( int64_t ), 1, file);

        for ( int i = 0; i < curCouunt; i++ )
        {
			std::string sstr;
            fread( &N, sizeof( int64_t ), 1, file );
            sstr.resize( N );
            fread( (void*)sstr.data(), N * sizeof( char ), 1, file );
            Add_Item( sstr );
            //	//std::cout << sstr;
        }
	}

	void Add_Item( std::string List_Data)
	{
		ListNode* temp = new ListNode;

		temp->data = std::move( List_Data );

		if (count != 0)
		{
			if (count == 1)
			{
				head->next = temp;
				temp->prev = head;
			}
			else
			{
				temp->prev = head->rand;
				head->rand->next = temp;
			}
			temp->rand = nullptr;
			head->rand = temp;
			tail = temp;
		}
		else
		{
			temp->prev = nullptr;
			temp->next = nullptr;
			temp->rand = nullptr;
			head = temp;
		}
		count++;
	}

private:
	ListNode* head;
	ListNode* tail;
	int64_t count;
};

int main()
{
	std::vector<std::string> Text_Mass = { "AA", "BB", "CC", "DD" };

	const char* path = "input.bin";
	List inputList(Text_Mass);
	List outputList;
	FILE* file = fopen(path, "wb");

	if (file != nullptr)
	{
		inputList.Serialize(file);
	}

	if (file != 0)
	{
		fclose(file);
	}

	file = fopen(path, "rb");
	if (file != nullptr)
	{
		outputList.Deserialize(file);
	}

	return 0;
}